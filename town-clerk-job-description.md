# Town Clerk Job Description

The position of town clerk is complex, requiring the clerk to be familiar with the requirements created by the New Hampshire RSAs (Revised Statutes Annotated) that are within the scope of the clerk's duties and to review and understand various documents that are presented to them by citizens to show eligibility for licenses and registrations. The position requires cooperation and a spirit of teamwork with other town officials. The role demands versatility, quick decision-making, accuracy, and patience.

## Requirements and Specifications
The town clerk:

- is elected and serves for a term of one year.
- _must_ be a resident of Orange, NH.
- _may not_  hold the office of treasurer, supervisor of the checklist, or town auditor.

The compensation of the town clerk is set by the town meeting in its capacity as a legislative body and currently comprises a set salary plus statutory fees collected. (It is possible for the clerk to be compensated by salary only with the statutory fees remaining with the town.)

The town clerk is not an employee of the town and is not subject to the personnel policies of the town with respect to vacation time, sick time, or hours of work per week. The clerk may set their own office hours and take what vacation and sick time they feel they can.

## General Administrative Duties
The town clerk:

- responds to email and phone calls from residents.
- submits budget requests to the selectmen for funds to attend trainings and other items necessary to run the office.
- safeguards the financial assets that they collect for the town.
- deposits fees collected with the treasurer at least weekly in accordance with RSA 261:165 (Accounting for Receipts of Permit Fees), after which the clerk submits an invoice to the treasurer for payment of the fees.
- appoints a deputy town clerk subject to the approval of the selectmen.
- attends trainings throughout the year, annual workshops, and an annual conference. Some travel is required.
- processes tax appeals and abatement applications.
- works with various software programs which require some training and troubleshooting. 
- acts as the local “information booth,” addressing concerns and answering questions from residents, businesses, visitors, elected officials, coworkers and others. Questions typically take the form of: "How do I?," "Where is?," "When is?,", and "Who is?."

## Public Records and Reporting
A vital role of the town clerk is as keeper of all of the town’s public records. Many statutes require that in order to be effective, ordinances, by-laws, regulations, and warrants must be on file with the town clerk.

The town clerk:

- keeps all books, records, papers, vouchers, and documents in the possession of any officers, committee, or board of officers of the town, and which are not needed elsewhere. 
- is well versed in RSA 91-A, the Right-to-Know Law, which requires that records intended to be disclosed to the public are made easily accessible.
- keeps a chronological record of births, marriages, deaths, and personal name changes reported to their office and transmits a copy of these records to the state registrar.
- issues to any applicant a certified copy of any record in the office relative to births, marriages, and deaths and may charge a fee as permitted by statute.
- reports to various state agencies and others within the legally specified timeframes after the annual town meeting.
- records, in the official records of the town, all votes of the town meeting and certifies the town meeting minutes. 
- collects and files land use records.
- purges old documents according to a retention schedule.

## Registrations and Licensing
The town clerk:

- registers motor vehicles, which often requires regular communication with the Department of Motor Vehicles.
- handles motor vehicle title transfers, including new purchases which require interactions with auto dealerships.
- processes e-file payments made via the internet and ensures that the appropriate accounts are credited.
- separately processes and accounts for in-person payments.
- prepares and reconciles monthly income ledger and reports results to treasurer.
- understands the use of certificates of title, which vehicles are exempted from the law, when transfer credits must be given, and what registration fees must be charged.
- registers dogs.

Town clerks that are, at their option, appointed as municipal agents must attend training sessions, must agree to the rules as enacted by the Department of Motor Vehicles, and must secure a bond in favor of the state.

## Elections
The town clerk has many responsibilities before, during, and after the elections and annual town meetings and plays a very visible role in the coordination of elections and the reporting of election results. They are the primary town official in charge of state, federal, and school board election preparation and for ensuring that election procedures are followed and that results are recorded and delivered to the appropriate governing body.

The town clerk:

- serves as an election officer responsible for conducting all local, state, and national elections.
- registers voters.
- keeps up to date with all of the election legislation that has been passed since the last election.
- presides at the polling place.
- interacts with the office of the Secretary of State of New Hampshire.
- files the Grand List as prepared by the keepers of the checklist. 
- prepares voter registration data for the keepers of the checklist.
- accepts declarations of candidacy and decides whether the person filing meets the requirements for office with respect to residency and filing dates.
- prepares the ballots for town elections.
- holds legally required additional office hours to accept ballots.
- inspects ballots for state elections sent to the clerk by the Secretary of State of NH in order to verify that they are the correct ones for the town.
- ensures that ballots for school board elections are available on election day.
- administers the absentee ballot process for both state and town elections.
- certifies election results.
- disseminates official results to media outlets who have requested them.
- sets up the time for election recounts if necessary.

## Estimated Effort Hours
Eight hours of effort per week is typical for Orange in 2021. Following is a breakdown of estimated time required for typical duties.

- Registrations, licensing, vital records, in-person interaction with residents: 2.5 hours/week
- Meeting residents outside of regular office hours: 0.5 hours/week
- Training and research (software, RSAs, DMV questions): 0.5 hours/week
- Email and phone communications: 1 hour/week
- Local travel to banks and post office: 0.5 hour/week
- Preparation of outgoing mail and bank deposits, reviewing income reports, filing: 2 hours/week
- Reconciling the town clerk's accounting ledger and reporting to treasurer: 0.75 hours/week
- Miscellaneous office management, inventory and supplies, office upkeep, interaction with deputy clerk, preparation of forms and documents: 0.25 hours/week
- During election season additional time is required for absentee ballots, voter registration, preparation of the Grand List, paperwork, and formal reporting: 2 hours/week
